package id.co.iconpln.mylistapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_detail_hero.*

class DetailHeroActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_DESC = "extra_desc"
        const val EXTRA_IMAGE_URL = "extra_image_url"

        const val EXTRA_HERO = "extra_hero"
    }

    private lateinit var hero: Hero

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_hero)

        setupActionBar()
        initIntentExtra()
        displayHeroDetail()
    }

    private fun initIntentExtra() {
        hero = intent.getParcelableExtra(EXTRA_HERO)
    }

    private fun setupActionBar() {
        supportActionBar?.title = "Detail Hero"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun displayHeroDetail() {
//        val bundle = intent.extras

//        tvHeroDetailName.text = intent.getStringExtra(EXTRA_NAME)
//        tvHeroDetailDesc.text = intent.getStringExtra(EXTRA_DESC)

        tvHeroDetailName.text = hero.name
        tvHeroDetailDesc.text = hero.desc

        Glide.with(this)
            .load(hero.phooto)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
            )
            .into(ivHeroDetailImage)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                false
            }
        }
    }
}
