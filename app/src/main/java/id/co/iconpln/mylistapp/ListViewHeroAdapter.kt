package id.co.iconpln.mylistapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class ListViewHeroAdapter(val context: Context, val listHero: ArrayList<Hero>) : BaseAdapter() {
    override fun getView(index: Int, view: View?, viewGroup: ViewGroup?): View {
        val viewLayout = LayoutInflater.from(context)
            .inflate(R.layout.item_list_hero, viewGroup, false)

        val viewHolder = ViewHolder(viewLayout)
        val hero = getItem(index) as Hero
        viewHolder.bind(context, hero)

        return viewLayout
    }

    override fun getItem(index: Int): Any {
        return listHero[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return listHero.size
    }

    private inner class ViewHolder(view: View) {
        private val tvHeroName: TextView = view.findViewById(R.id.tv_hero_name)
        private val tvHeroDesc: TextView = view.findViewById(R.id.tv_hero_desc)
        private val ivHeroPhoto: ImageView = view.findViewById(R.id.iv_hero_image)

        fun bind(context: Context, hero: Hero) {
            tvHeroName.text = hero.name
            tvHeroDesc.text = hero.desc

            Glide.with(context)
                .load(hero.phooto)
                .into(ivHeroPhoto)

            //TODO: Buat proyek List seperti hero jangan ada yang sama
        }
    }
}