package id.co.iconpln.mylistapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_list_hero.*

class MainActivity : AppCompatActivity() {

    private val listHero: ListView
        get() = lv_list_hero

    private val list: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //loadListArrayAdapter()
        loadListBaseAdapter(this)
        setListItemClickListener(listHero)
    }

    private fun loadListBaseAdapter(context: Context) {
        list.addAll(HeroesData.listDataHero)

        val baseAdapter = ListViewHeroAdapter(context, list)
        listHero.adapter = baseAdapter
    }

    fun loadListArrayAdapter() {
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getDataHero())
        listHero.adapter = adapter
    }

    fun getDataHero(): Array<String> {
        val hero = arrayOf(
            "Bung Karno",
            "Bung Hatta",
            "Soeharto",
            "Diponegoro",
            "Imam Bonjol",
            "Ki Hajar Dewantara"
        )
        return hero
    }

    private fun setListItemClickListener(listView: ListView) {
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, index, l ->
                Toast.makeText(this@MainActivity, "Pahlawan ${list[index].name}", Toast.LENGTH_LONG)
                    .show()
                showDetailHero(list[index])
            }
    }

    private fun showDetailHero(hero: Hero) {
        val detailHeroIntent = Intent(this, DetailHeroActivity::class.java)

        detailHeroIntent.putExtra(DetailHeroActivity.EXTRA_HERO, hero)

//        detailHeroIntent.putExtra(DetailHeroActivity.EXTRA_NAME, hero.name)
//        detailHeroIntent.putExtra(DetailHeroActivity.EXTRA_DESC, hero.desc)
//        detailHeroIntent.putExtra(DetailHeroActivity.EXTRA_IMAGE_URL, hero.phooto)


        startActivity(detailHeroIntent)
    }
}
